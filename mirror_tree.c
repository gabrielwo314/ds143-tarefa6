#include <stdio.h>
#include <stdlib.h>

typedef struct arvore {
  int info;
  struct arvore *esq;
  struct arvore *dir;
} Arvore;

int buscar (Arvore *a, int v) {
  if (a == NULL) { return 0; } 
  else if (v < a->info) {
    return buscar (a->esq, v);
  }
  else if (v > a->info) {
    return buscar (a->dir, v);
  }
  else { return 1; } 
}

Arvore* inserir (Arvore *a, int v) {
  if (a == NULL) {
    a = (Arvore*)malloc(sizeof(Arvore));
    a->info = v;
    a->esq = a->dir = NULL;
  }
  else if (v < a->info) {
    a->esq = inserir (a->esq, v);
  }
  else { a->dir = inserir (a->dir, v); }
  return a;
}

void in_order(Arvore *a){
  if(!a)
    return;
  in_order(a->esq);
  printf("%d ",a->info);
  in_order(a->dir);
}

Arvore *init() {
  Arvore *p;
  p = (Arvore *) malloc(sizeof(Arvore));
  if (!p) {
    printf("Problema de alocação");
    exit(0);
  }
  p->esq = NULL;
  p->dir = NULL;

  return p;
}

int eh_espelho(Arvore *a, Arvore *b) {
  if(!a && !b )
    return 1;
  if (!a || !b || a->info != b->info)
    return 0;

  return (eh_espelho((a->esq), (b->dir)) &&
    eh_espelho((a->dir), (b->esq)));
}

Arvore *cria_espelho(Arvore **t) {
  Arvore *new, *dir, *esq;
  if (!(*t))
    return NULL;

  esq = cria_espelho(&(*t)->esq);
  dir = cria_espelho(&(*t)->dir);

  new = init();
  new->info = (*t)->info;
  new->esq = dir;
  new->dir = esq;
  return new;
}

void print2DUtil(Arvore *root, int space) {
  // Base case
  if (root == NULL)
    return;

  // Increase distance between levels
  space += 5;

  // Process right child first
  print2DUtil(root->dir, space);

  // Print current node after space
  // count
  printf("\n");
  for (int i = 5; i < space; i++)
      printf(" ");
  printf("%d\n", root->info);

  // Process left child
  print2DUtil(root->esq, space);
}

void print2D(Arvore *root) {
  // Pass initial space count as 0
  print2DUtil(root, 0);
}

int main(){
  Arvore *a, *esp;

  a = inserir(NULL,50);
  a = inserir(a,30);
  a = inserir(a,90);
  a = inserir(a,20);
  a = inserir(a,40);
  a = inserir(a,95);
  a = inserir(a,10);
  a = inserir(a,35);
  a = inserir(a,45);
  a = inserir(a,37);

  print2D(a);
  printf("\n");
  printf("-------------------------------------------");
  printf("\n");

  esp = cria_espelho(&a);
  print2D(esp);
  printf("\n");

  if(eh_espelho(a, esp)) printf("É espelho\n");
}
